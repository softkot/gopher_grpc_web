module gitlab.com/softkot/gopher_grpc_web

go 1.17

require github.com/johanbrandhorst/protobuf v0.7.1

require (
	github.com/gopherjs/gopherjs v1.17.2 // indirect
)
