ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
GOPHERVERSION:=v1.17.2
GOVERSION:=go1.17.9
GOPHERPROTO:=v0.7.1
GOPHERJS_GOROOT:=$(shell $(GOVERSION) env GOROOT)

.PHONY: init
init:
	go install golang.org/dl/$(GOVERSION)@latest
	$(GOVERSION) download
	$(GOVERSION) install github.com/johanbrandhorst/protobuf/protoc-gen-gopherjs@$(GOPHERPROTO)
	$(GOVERSION) install github.com/gopherjs/gopherjs@$(GOPHERVERSION)

.PHONY: proto
proto:
	rm -rf client
	mkdir -p client
	protoc -I=proto --gopherjs_out=import_path=client:client auth.proto

.PHONY: tidy
.ONESHELL:
tidy:
	alias go="$(GOVERSION)"
	go mod tidy

.PHONY: build
.ONESHELL:
build:
	alias go="$(GOVERSION)"
	export GOPHERJS_GOROOT=$(GOPHERJS_GOROOT)
	gopherjs build -m gitlab.com/softkot/gopher_grpc_web -o gophger_grpc_web.js

.PHONY: serve
.ONESHELL:
serve:
	alias go="$(GOVERSION)"
	export GOPHERJS_GOROOT=$(GOPHERJS_GOROOT)
	gopherjs serve gitlab.com/softkot/gopher_grpc_web